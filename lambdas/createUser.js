const Responses = require('./common/apiResponses')
const Dynamo = require('./common/dynamo')
const Salt = require('./common/saltLib')

const userTableName = process.env.userTableName
const saltTableName = process.env.saltTableName

exports.handler = async event => {
  console.log(event)
  if(!event.pathParameters || !event.pathParameters.email) {
    return Responses._400({error: "No parameters or no email in path"})
  }

  let email = event.pathParameters.email

  if(!validateEmail(email)) {
    return Responses._400({error: "Invalid email"})
  }

  const user = JSON.parse(event.body)
  if (user.password.length < 8) {
    return Responses._400({error: "Password must have at least 8 charaters"})
  }
  console.log(`user: ${JSON.stringify(user)}`)
  user.email = email

  const userExists = await Dynamo.get(email, userTableName).catch(err => {
    console.log("user not found!", err)
    return null
  });

  if(userExists) {
    return Responses._400({error: "Email already exists!"})
  }

  const saltString = Salt.getSalt();
  user.password = Salt.getHash(user.password, saltString)

  const saltID = {
    email: user.email,
    salt: saltString
  }

  const insertSalt = await Dynamo.write(saltID, saltTableName).catch(err => {
    console.log(`Error: Couldn´t write salt to database ${saltTableName}`, err)
    return null
  })

  if(!insertSalt) {
    return Responses._400({error: "Couldn´t write to database"})
  }

  const newUser = await Dynamo.write(user, userTableName).catch(err => {
    console.log(`Error: Couldn´t write ${user} to database ${userTableName}`, err)
    return null
  })

  if (!newUser) {
    return Responses._400({error: `Failed to write ${user} to database ${userTableName}`})
  }

  return Responses._200({newUser})
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}