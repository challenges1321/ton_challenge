const Responses = require('./common/apiResponses')
const Dynamo = require('./common/dynamo')
const Salt = require('./common/saltLib')

const userTableName = process.env.userTableName
const saltTableName = process.env.saltTableName

exports.handler = async event => {
  console.log(event)
  if(!event.pathParameters || !event.pathParameters.email) {
    return Responses._400({error: "No parameters or no email in path"})
  }

  let email = event.pathParameters.email
  let password = event.queryStringParameters.password

  const user = await Dynamo.get(email, userTableName).catch(err => {
    console.log("Error: user not found!", err)
    return null
  });

  if (!user) {
    return Responses._400({error: "Failed to get user by email"})
  }

  const getSalt = await Dynamo.get(email, saltTableName).catch(err => {
    console.log(`Error: Couldn´t write salt to database ${saltTableName}`, err)
    return null
  })

  if(!getSalt) {
    return Responses._400({error: "Couldn´t get password from database"})
  }
  let hashPassword = Salt.getHash(password, getSalt.salt)

  if (user.password == hashPassword) {
    return Responses._200({user})
  } else {
    return Responses._400({error: "Wrong password"})
  }
}