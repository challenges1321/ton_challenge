const AWS = require('aws-sdk')

const documentClient = new AWS.DynamoDB.DocumentClient()

const Dynamo = {
  async get(email, TableName) {
    const params = {
      TableName,
      Key: {
        email
      }
    }

    const data = await documentClient.get(params).promise()

    if(!data || !data.Item) {
      throw Error(`Error fetching the email ${email} from ${TableName}`)
    }
    console.log(data)
    return data.Item
  },

  async write(data, TableName) {
    if(!data.email) {
      throw Error('no email on the data')
    }

    const params = {
      TableName,
      Item: data
    }

    const res = await documentClient.put(params).promise()

    if (!res) {
      throw Error(`Error inserting ${email} in table ${TableName}`)
    }

    return data
  }
}

module.exports = Dynamo