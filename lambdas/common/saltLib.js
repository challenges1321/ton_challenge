const crypto = require('crypto')

const Salt = {
  getSalt(length=255) {
    let str = [];
    let digits = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_"

    for (let i = 0; i < length; i++) {
      str[i] = digits.substr(Math.floor(Math.random() * digits.length - 1), 1);
    }

    return str.join("")
  },
  getHash(pwd, salt) {
    let hash = crypto.pbkdf2Sync(pwd,salt, 100000, 128, 'sha512')
    return Buffer.from(hash, 'hex').toString('base64')
  }
}

module.exports = Salt