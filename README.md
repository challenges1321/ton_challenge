# APIS:

https://h6gz9pcqs3.execute-api.us-east-1.amazonaws.com/dev/proxy-hit

https://h6gz9pcqs3.execute-api.us-east-1.amazonaws.com/dev/proxy-get

https://h6gz9pcqs3.execute-api.us-east-1.amazonaws.com/dev/proxy-reset

https://h6gz9pcqs3.execute-api.us-east-1.amazonaws.com/dev/get-user/johndoe@gmail.com?password=1234

https://h6gz9pcqs3.execute-api.us-east-1.amazonaws.com/dev/create-user/johndoe@gmail.com

# Docs

![text](/docs/apidoc.png)
